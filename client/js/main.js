/**
 * Created by marcpeters on 2016-10-21.
 */

const React = require('react')
const ReactDOM = require('react-dom')
const pui = require('./components/ProxicleUI')

ReactDOM.render(<pui.ProxicleUI></pui.ProxicleUI>, document.getElementById('app'))