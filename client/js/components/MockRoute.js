/**
 * Created by marcpeters on 2016-10-26.
 */

const React = require('react')
const MockRouteEditorControls = require('./MockRouteEditorControls')
const EditableTextBox = require('./EditableTextBox')
const EditableTextArea = require('./EditableTextArea')
const httpStatusCodes = require('json!../../data/http_status_codes.json')
const jsonMarkup = require('json-markup')
const _ = require('lodash')

class MockRoute extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showDetails: false,
      editing: false
    }
  }

  componentDidMount() {
    this.resetFormValues()
  }

  render() {

    const statusCodeDescription = '(' + ((_.find(httpStatusCodes, { "code": _.get(this, 'state.newStatusCode', '').toString() }) || {}).phrase) + ')'

    return <li>
      <a className="link" onClick={this.toggleShowDetails.bind(this)}>
        {this.props.data.path}
      </a>

      <div className={this.state.showDetails? 'route-details' : 'route-details hidden'}>
        <MockRouteEditorControls
          showHideDetailsBtn={false}
          onEditStart={this.startOrCancelEdit.bind(this)}
          onEditCancel={this.startOrCancelEdit.bind(this)}
          onSave={this.onSave.bind(this)}
          onDelete={this.deleteRoute.bind(this)}
          onHideDetails={this.toggleShowDetails.bind(this)}
          editing={this.state.editing}
        />
        <table>
          <tbody>
          <tr>
            <td># times returned:</td>
            <td>{this.props.data.timesReturned}</td>
          </tr>
          <tr>
            <td>status code:</td>
            <td>
              <EditableTextBox
                editing={this.state.editing}
                onChange={this.handleFormDataChange('newStatusCode').bind(this)}
                staticValue={`${this.state.newStatusCode} ${statusCodeDescription}`}
                editingValue={this.state.newStatusCode}
              />
            </td>

          </tr>
          <tr>
            <td>delay:</td>
            <td>
              <EditableTextBox
                editing={this.state.editing}
                onChange={this.handleFormDataChange('newDelay').bind(this)}
                staticValue={`${this.state.newDelay}ms`}
                editingValue={this.state.newDelay}
              />
            </td>
          </tr>
          <tr>
            <td>response body:</td>
            <td>
              {/*<span className={this.state.editing? 'hidden' : ''} dangerouslySetInnerHTML={{__html: jsonMarkup(this.props.data.responseBody)}}>*/}

              {/*</span>*/}
              {/*<textarea rows="20" ref={(input) => this.textArea = input} className={this.state.editing? '' : 'hidden'} onKeyDown={this.handleTextAreaKeyPress.bind(this)} onChange={this.handleFormDataChange('newResponseBody').bind(this)} value={this.state.newResponseBody}/>*/}
              <EditableTextArea
                editing={this.state.editing}
                onContentChanged={this.handleFormDataChange('newResponseBody').bind(this)}
                staticValue={this.state.newResponseBody}
                editingValue={this.state.newResponseBody}
                isJSOL="true"
              />
            </td>
          </tr>

          </tbody>
        </table>

        <span className="error">
          {this.state.error}
        </span>

        <MockRouteEditorControls
          showHideDetailsBtn="true"
          onEditStart={this.startOrCancelEdit.bind(this)}
          onEditCancel={this.startOrCancelEdit.bind(this)}
          onSave={this.onSave.bind(this)}
          onDelete={this.deleteRoute.bind(this)}
          onHideDetails={this.toggleShowDetails.bind(this)}
          editing={this.state.editing}
        />
      </div>
    </li>
  }

  onSave() {
    try {
      var newResponseBody = JSON.parse(this.state.newResponseBody)
    }
    catch (err) {
      this.setState({error: `Invalid JSON: ${err.message}`})
      return
    }

    this.props.onSave({
      url: this.props.data.path,
      statusCode: this.state.newStatusCode,
      body: newResponseBody,
      delay: this.state.newDelay
    })
    this.setState({ editing: false})
    this.clearErrorMessage()
  }

  handleFormDataChange(statePropertyName) {
    return (value) => {
      var newState = {}
      newState[statePropertyName] = value
      this.setState(newState)
    }
  }

  // handleTextAreaKeyPress(event) {
  //   if(event.keyCode === 9) { // tab was pressed
  //     console.log('tab pressed')
  //     console.log(event)
  //     // get caret position/selection
  //     var start = this.textArea.selectionStart;
  //     var end = this.textArea.selectionEnd;
  //
  //     var value = event.target.value
  //
  //     var newValue = value.substring(0, start)
  //       + "\t"
  //       + value.substring(end)
  //
  //     this.setState({ newResponseBody: newValue }, () => {
  //       // put caret at right position again (add one for the tab)
  //       this.textArea.selectionStart = this.textArea.selectionEnd = start + 1;
  //     })
  //
  //     // prevent the focus lose
  //     event.preventDefault();
  //   }
  // }

  toggleShowDetails() {
    this.setState({ showDetails: !this.state.showDetails, editing: false })
  }

  resetFormValues() {
    this.setState({
      newDelay: this.props.data.delay,
      newStatusCode: this.props.data.statusCode,
      //newResponseBody: JSON.stringify(this.props.data.responseBody, null, 2)
      newResponseBody: this.props.data.responseBody
    })
  }

  startOrCancelEdit() {
    this.setState({ editing: !this.state.editing }, () => {
      if (!this.state.editing) {
        this.resetFormValues()
        this.clearErrorMessage()
      }
    })
  }

  clearErrorMessage() {
    this.setState({error: null})
  }

  deleteRoute() {
    this.props.onDelete(this.props.data.path)
  }
}

module.exports = MockRoute