/**
 * Created by marcpeters on 2016-11-07.
 */

const React = require('react')
const buttonColorMap = require('json!../../data/button_colors.json')
const _ = require('lodash')

class Button extends React.Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.state.buttonColorClass = _.get(buttonColorMap, this.props.color, '')
  }

  render() {
    return this.props.hidden ? null :
      <a className={`${this.state.buttonColorClass} btn btn-inline`} onClick={this.props.onClick}>
        {this.props.children}
      </a>
  }

}

module.exports = Button