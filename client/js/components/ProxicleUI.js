/**
 * Created by marcpeters on 2016-10-26.
 */

const React = require('react')
const request = require('superagent')
const _ = require('lodash')

const MockRoute = require('./MockRoute.js')
const MockRouteAdder = require('./MockRouteAdder.js')
const InstanceSelector = require('./InstanceSelector.js')


class ProxicleUI extends React.Component {

  constructor(props) {
    super(props)

    this.allInstances = [
      "http://localhost:5001" ]

    this.state = {
      allInstances: this.allInstances,
      selectedInstance: null,
      routes: [],
      message: ''
    }

    this.interval = null
  }

  componentDidMount() {
    this.instanceChanged(this.allInstances[0])
  }

  render() {
    const proxiedRoutes = this.state.routes.map((route) => {
      //return null
      return <MockRoute data={route} onDelete={this.deleteRoute.bind(this)} onSave={this.saveRoute.bind(this)} key={route.path}/>
    })

    return <div>
      <InstanceSelector allInstances={this.state.allInstances} onInstanceChanged={this.instanceChanged.bind(this)}/>
      <MockRouteAdder/>
      <div>
        <ul>
          {proxiedRoutes}
        </ul>
      </div>
      {this.state.message}
      <span className="error">{this.state.error}</span>
    </div>
  }

  getProxiedRoutes() {

    //Remember what proxicle instance was selected when this function was called
    var proxicleInstance = this.state.selectedInstance

    request.get(`${this.state.selectedInstance}/proxy/listMockResponses?detailed=true`).end((err,res) => {
      if(this.state.selectedInstance != proxicleInstance) {
        console.log('Instance changed while getting mock responses.  Discarding data.')
        return
      }

      if (err) {
        console.log('Error while fetching the list of mock responses:')
        console.log(err)
        this.setState({error: 'An error occurred while fetching the list of mock responses.'})
      }

      this.setState({message: Object.keys(res.body).length === 0 && res.body.constructor === Object ? <em>No mock responses configured</em> : "",
        routes: Object.keys(res.body).map((key) => {
          var route = Object.assign({}, res.body[key])
          route.path = key // URL part of route
          return route
        }),
        error: null
      })
    })
  }

  instanceChanged(url) {

    if(this.interval) {
      clearInterval(this.interval)
      this.interval = null
    }

    this.setState({selectedInstance: url, routes: [], message: '', error: null}, () => {
      this.getProxiedRoutes()
      this.interval = setInterval(this.getProxiedRoutes.bind(this), 1000, url)
    })

  }

  deleteRoute(routePath) {
    var predicate = _.matches({ path: routePath })

    if(!_.some(this.state.routes, predicate)) {
      this.setState({error: `Unable to find route: ${routePath} in the collection`})
      return
    }

    var newRoutes = _.filter(this.state.routes, _.negate(predicate))
    this.setState({routes: newRoutes})

    request.del(`${this.state.selectedInstance}/proxy/clearMockResponse`)
    .send({url: routePath})
    .end()
  }

  saveRoute(options) {
    console.log('Save route called')
    console.log(options)

    request.put(`${this.state.selectedInstance}/proxy/setMockResponse`)
    .send(options)
    .end((err, res) => {
      if(err) {
        console.error('Error when saving mock response:')
        console.error(err)
        return
      }

      this.getProxiedRoutes()
    })
  }
}

module.exports = { ProxicleUI: ProxicleUI }