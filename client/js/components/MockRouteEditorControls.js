/**
 * Created by marcpeters on 2016-10-26.
 */

const Button = require('./Button.js')

const React = require('react')

class MockRouteEditorControls extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return <div className="route-details-controls">
      <div className={this.props.editing? 'hidden' : ''}>
        <Button color="brown" hidden={!this.props.showHideDetailsBtn} onClick={this.props.onHideDetails}>Hide Details</Button>
        <Button color="brown" onClick={this.props.onEditStart}>Edit</Button>
        <Button color="red" onClick={this.props.onDelete}>Delete</Button>
      </div>
      <div className={this.props.editing? '' : 'hidden'}>
        <Button color="green" onClick={this.props.onSave}>Save</Button>
        <Button color="red" onClick={this.props.onEditCancel}>Cancel</Button>
      </div>
    </div>
  }
}

module.exports = MockRouteEditorControls