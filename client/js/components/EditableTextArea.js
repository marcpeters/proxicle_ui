/**
 * Created by marcpeters on 2016-11-07.
 */

const React = require('react')
const jsonMarkup = require('json-markup')


class EditableTextArea extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      selectionStart: 0,
      selectionEnd: 0
    }
  }

  render() {
    if(!this.props.editing) {
      if(this.props.isJSOL) {
        return <span
          dangerouslySetInnerHTML={{__html: jsonMarkup(this.props.staticValue)}}>
        </span>
      }
      else {
        return <span>
          {this.props.staticValue}
        </span>
      }
    }
    else {
      return <textarea rows="20"
               onKeyDown={this.handleTextAreaKeyPress.bind(this)}
               onChange={this.onChange.bind(this)}
               value={undefined}
               ref={(input) => this.textArea = input}/>
    }
  }

  // Workaround for textarea content changes triggering the caret to move to the end
  // of the content.
  // See http://stackoverflow.com/questions/28922275/in-reactjs-why-does-setstate-behave-differently-when-called-synchronously/28922465#28922465
  componentDidUpdate (prevProps) {
    var node = this.textArea
    if(!node) return
    var oldLength = node.value.length
    var oldIdx = node.selectionStart
    node.value = this.props.isJSOL ? JSON.stringify(this.props.editingValue, null, 2) : this.props.editingValue
    var newIdx = Math.max(0, node.value.length - oldLength + oldIdx)
    node.selectionStart = node.selectionEnd = newIdx
  }

  handleTextAreaKeyPress(event) {

    if(event.keyCode === 9) { // tab was pressed

      // get caret position or selection
      var start = this.textArea.selectionStart;
      var end = this.textArea.selectionEnd;

      var newValue = event.target.value.substring(0, start)
        + "\t"
        + event.target.value.substring(end)

      // prevent loss of focus
      event.preventDefault();

      // Notify the parent component of the content change
      if (this.props.isJSOL) {
        newValue = JSON.parse(newValue)
      }
      this.props.onContentChanged(newValue)
    }
  }

  onChange(event) {
    // console.log(event.target.value)
    var newValue = event.target.value
    if (this.props.isJSOL) {
      newValue = JSON.parse(newValue)
    }
    this.props.onContentChanged(newValue)
  }

}

module.exports = EditableTextArea