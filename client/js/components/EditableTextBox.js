/**
 * Created by marcpeters on 2016-11-07.
 */

const React = require('react')

class EditableTextBox extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return !this.props.editing ?
    <span>
      {this.props.staticValue}
    </span>
      :
    <input type="text" onChange={this.handleFormDataChange.bind(this)} value={this.props.editingValue}/>
  }

  handleFormDataChange(event) {
    this.props.onChange(event.target.value)
  }

}

module.exports = EditableTextBox