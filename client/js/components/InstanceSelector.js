/**
 * Created by marcpeters on 2016-10-26.
 */

const React = require('react')

class InstanceSelector extends React.Component {

  constructor(props) {
    super(props)
    this.options = this.props.allInstances.map((instance) => {
      return <option value={instance} key={instance}>{instance}</option>
    })
  }

  instanceChanged(e) {
    this.props.onInstanceChanged(e.target.value)
  }

  render() {
    return <div className="instance-selector">
      <select onChange={this.instanceChanged.bind(this)}>
        {this.options}
      </select>
    </div>
  }
}

module.exports = InstanceSelector