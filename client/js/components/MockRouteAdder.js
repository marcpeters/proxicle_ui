const React = require('react')

class MockRouteAdder extends React.Component {

	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return <div>
			<span className={this.state.expanded? 'hidden' : 'link'} onClick={this.onExpand.bind(this)}>+ Add mock route</span>
			<div className={this.state.expanded? '' : 'hidden'}>
				Add route here
			</div>
		</div>
	}

	onExpand() {
		this.setState({expanded: true})
	}

}

module.exports = MockRouteAdder