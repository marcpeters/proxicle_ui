/**
 * Created by marcpeters on 2016-10-21.
 */

module.exports = {
  entry: "./client/js/main.js",
  output: {
    path: "./public/js",
    filename: "bundle.js"
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
      { test: /\.css$/, loader: "style!css" }
    ]
  }
};